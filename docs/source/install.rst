
Installation
============

To install the most recent version of :mod:`mqttstore`, use :mod:`pip`:

.. code-block:: sh

    # make sure you have a recent version of setuptools
    python3 -m pip install --user --upgrade setuptools
    # from PyPi
    python3 -m pip install --user --upgrade mqttstore
    # or from the repository root
    python3 -m pip install --user --upgrade .

This will install :mod:`mqttstore`'s dependencies from `PyPi - the Python
Package Index`_.

Depending on your setup it might be necessary to install the :mod:`pip` module
first:

.. code-block:: sh

    # Debian/Ubuntu
    sudo apt-get install python3-pip

Or see `Installing PIP`_.

.. _Installing PIP: https://pip.pypa.io/en/stable/installing/
.. _PyPI - the Python Package Index: https://pypi.org/
