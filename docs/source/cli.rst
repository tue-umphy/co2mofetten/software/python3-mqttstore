Command-Line Interface
======================

:mod:`mqttstore` comes with a :doc:`cli`.

Invocation
++++++++++

:mod:`mqttstore`'s :doc:`cli` is invoked from the command-line by launching the
``mqttstore`` command:

.. code-block:: sh

   mqttstore
   # Usage: mqttstore [OPTIONS] COMMAND [ARGS]...
   #
   #  Store MQTT data
   # ...

If running the above fails with something sounding like ``command not found:
mqttstore``, you may either always run ``python3 -m mqttstore`` instead of a
plain ``mqttstore`` or add the directory ``~/.local/bin`` to your ``PATH``
environment variable by appending the following line to your shell's
configuration (probabily ``.bashrc`` in your home directory):

.. code-block:: sh

   export PATH="$HOME/.local/bin:$PATH"

The :doc:`cli` is based on `Click`_ and requires a subcommand to be specified.
The available subcommands will be explained throughout this page.

Help
----

A help page is available for both the ``mqttstore`` command and each
subcommand. To access it, pass the ``--help`` (or ``-h``) option to it:

.. code-block:: sh

    mqttstore --help
    # Usage: mqttstore [OPTIONS] COMMAND [ARGS]...
    #
    #  Store MQTT data
    # ...
    mqttstore route_mqtt --help
    # Usage: mqttstore route_mqtt [OPTIONS]
    #
    #  Store MQTT data to an SQLite database
    # ...


.. _cli logging:

Logging
-------

If something is not working as expected or the :doc:`cli` seems to ”hang”, you
might want to check some more verbose logging output to see what's going on.
You can increase the verbosity of the :doc:`cli` by passing the ``--verbose``
(or ``-v``) option one or multiple times to the ``mqttstore`` command:

.. code-block:: sh

   mqttstore -v ...
   # a little more output
   # ...
   mqttstore -vvvv ...
   # VERY much logging output
   # ...

Analogously, if you'd like less verbosity, specify the ``--quiet`` (or ``-q``)
options one or more times.

.. hint::

   Keep in mind that you have to specify the logging options **to the**
   ``mqttstore`` **command, BEFORE any subcommand**!

.. _cli mqtt store sqlite:

Storing to an SQLite database
+++++++++++++++++++++++++++++


The parsing mechanisms and storage instructions are specified in a
configuration file which is then passed to the subcommand via the ``--config``
(or ``-c``) option:

.. code-block:: sh

    mqttstore sqlite -c /path/to/config.conf

Database Settings
-----------------

The ``[database]`` section in the configuration file can be used to configure
the database.

``file``
........

The database file to use.

``time_column``
...............

What name to use for the column that contains the time when the dataset is
recieved. Defaults to ``time``.

``bundle_interval``
...................

By default, the incoming data is appended to the database as it comes. However,
this might result in leaving a lot of empty cells in the database which is a
waste of database space. The ``bundle_interval`` field can be used to specify
an interval in seconds to group incoming data together. For example, if packets
of data from the broker comes in intervals of ``0.1`` seconds, you may specify
``bundle_interval = 0.1`` to group incoming data in that period together so
that it will be stored in the same row (if possible).

.. note::

   Keep in mind that when bundling incoming data you loose temporal precision.
   If you use ``bundle_interval = 0.1`` for example, stored rows will contain
   data that lies within the temporal interval of the ``time_column`` value
   up to ``bundle_interval`` seconds later.


MQTT Brokers
------------

Each MQTT broker to connect to is defined in its own ``[broker:some-title]``
section with fields that are described in the following. Generally, specifying
multiple values for a field can be done by specifying one value per line.

Connection Parameters
.....................

``hostname``, ``port``, ``ùsername`` and ``password`` are used for obvious
purposes, i.e. connecting to the broker.


``subscribe``
.............

This field contains the topics that the client should subscribe to. By default,
all topics (``#``) are subscribed to.

``parse_topic``
...............

This field contais regular expression patterns with `named capturing groups
<https://www.regular-expressions.info/named.html>`_ that are used to parse the
incoming topics. The matched values of the named capturing groups are then
available in the ``replace`` and ``store`` fields. Incoming topics that are not
matched by any regular expression defined here are ignored.

``parse_message``
.................

Analogously to ``parse_topic``, this field contains regular expressions to
parse incoming messages. By default, the first floating-point-number-looking
string is captured into a group named ``value``. The matched groups are merged
with the groups from ``parsed_topic``, whose values take precedence over
``parse_message``.


``replace``
...........

This field contains one or more replacement specifications (one by line) in the
format ``MATCH_REGEX = REPLACEMENT`` or ``MATCH_REGEX = REPLACEMENT --> KEY``.
Both ``MATCH_REGEX`` and ``REPLACEMENT`` are formatted with :any:`str.format`
as explained for the ``measurement`` field.  After formatting, ``MATCH_REGEX``
is interpreted as a regular expression that is attempted to match against all
named capturing groups from ``parse_topic`` and ``parse_message`` above. If
matched successfully, it will be replaced by ``REPLACEMENT``. If a ``KEY`` was
defined, instead of just replacing the values of the matching key of the named
capturing groups from above, that new key is created with the replaced string
and will be available in the following fields.  These replacements are
processed in order and may override each other.

This ``replace`` functionality is useful if your incoming MQTT topics don't
contain strings that exactly map to texts you want to have in your database. Or
if you want to manually add units to data that doesn't provide it.


``store``
.........

This field is the most important part of the configuration because it specifies
how the data is stored into the database. Lines have this general format:

.. code-block:: ini

   COLUMN1 = VALUE1 [[, COLUMN2 = VALUE2], COLUMN3 = ...] --> TABLE

All placeholders above (``COLUMN``, ``VALUE`` and ``TABLE``) are templates that
will be formatted with :any:`str.format` as explained for the ``measurement``
field. If any placeholder could not be substituted, the line is ignored.  The
**first** line where all placeholders are available is used to determine how to
store the incoming data. A row in the table ``TABLE`` is added where each
``COLUMN`` is set to the corresponding ``VALUE``.

An example broker configuration section might look like this:

.. code-block:: ini

    [broker:localhost]

    # hostname = localhost # defaults to localhost anyway
    # port = 1883 # defaults to 1883 anyway
    # credentials (if needed)
    # username = ...
    # password = ...

    # topics to subscribe to
    # (MQTT topic subscription syntax)
    # topic structure here e.g. home/PLACE/SENSOR/QUANTITY
    # but we are only interested in bme280 and scd30 indoor measurements
    # and data from our solar panel on the roof
    subscribe =
        home/+/+/+

    # regular expressions (one per line) to parse the incoming topics
    # The first one parses our home/PLACE/SENSOR/QUANTITY topics.
    # The second one parses all topics. This is handy if you also want to store
    # topics that don't match what you expect
    parse_topic =
        ^(?P<location>home)/(?P<place>[^/]+)/(?P<room>[^/]+)/(?P<sensor>[^/]+)/(?P<quantity>[^/]+)$
        ^(?P<topic>.*)$

    # regular expressions (one per line) to parse the incoming message
    # The first two expressions parse a numeric value with an optional unit
    # following after some whitespace. The last expression parses every
    # message. This is handy if you also want to store messages that don't
    # match what you expect.
    parse_message =
        ^(?P<data>[0-9.-]+)$
        ^(?P<data>[0-9.-]+)\s+(?P<unit>.*)$
        ^(?P<message>.*)$

    # We know that no unit comes along data published to the topic
    # home/bathroom/bme280/temperature, but we want the database to contain the
    # unit, so we create a new key 'bathroom_unit' that contains the right
    # unit.
    replace =
        ^home/bathroom/{sensor}/temperature$ = °C --> bathroom_unit

    # The first storage instruction line where all placeholders could be
    # substituted is used, so we put our custom {bathroom_unit} rule first
    # We want our home measurements to be stored into a table
    # called 'home PLACE SENSOR', so we use the template below
    store =
        # our custom bathroom rule comest first (column 'QUANTITY [°C]')
        {quantity} [{bathroom_unit}] = {data} --> {location} {place} {sensor}
        # data with a unit (column 'QUANTITY [UNIT]')
        {quantity} [{unit}]          = {data} --> {location} {place} {sensor}
        # data with a unit (column 'QUANTITY')
        {quantity}                   = {data} --> {location} {place} {sensor}
        # unsorted topics that come with data and unit
        topic = {topic}, data = {data}, unit = {unit} --> unsorted
        # unsorted topics that come with data but no unit
        topic = {topic}, data = {data} --> unsorted
        # unsorted topics that come with a strange message
        topic = {topic}, message = {message} --> unsorted



Now you can run the service:

.. code-block:: sh

   mqttstore sqlite -c /path/to/config.conf --file test.sqlite --bundle-interval 1

As no broker is defined in the configuration, a local broker is used.  You
might also want to turn verbose :ref:`cli logging` on to see what exactly is
going on under the hood.

Using `Mosquitto`_ we can publish something to the broker:

.. code-block:: sh

   # Publishing a packet of information in a short period of time smaller than
   # the bundle interval causes the data to be stored in a single row
   mosquitto_pub -t home/bathroom/bme280/humidity -m "45 %"
   mosquitto_pub -t home/bathroom/bme280/temperature -m "21.5" # no unit here
   mosquitto_pub -t home/bathroom/bme280/pressure -m "998.1 hPa"
   # If we instead publish the data slowly, one row per dataset is stored
   sleep 1.5 # wait until our bundle interval is over
   mosquitto_pub -t home/bathroom/bme280/humidity -m "46 %"
   sleep 1.5 # wait until our bundle interval is over
   mosquitto_pub -t home/bathroom/bme280/temperature -m "21.8" # no unit here
   sleep 1.5 # wait until our bundle interval is over
   mosquitto_pub -t home/bathroom/bme280/pressure -m "998.8 hPa"
   # Data that doesn't match our pattern is stored in a different table
   mosquitto_pub -t home/roof/solarpanel/works -m "yes"


We can dump our table using the ``sqlite3`` command-line utility:

.. code-block:: sh

   # loop over all table names
   sqlite3 test.sqlite .tables | perl -pe 's/\s\s+/\n/g' | while read table
   do
       echo "\nTable '$table':";
       # dump the table
       sqlite3 -column -header test.sqlite "select * from [$table];"
   done

.. code::

    Table 'home bathroom bme280':
    time                              bme280 pressure {hPa}  bme280 humidity {%}  bme280 temperature {°C}
    --------------------------------  ---------------------  -------------------  -----------------------
    2019-08-07 16:32:05.481722+00:00  998.1                  45                   21.5
    2019-08-07 16:32:06.997176+00:00                         46
    2019-08-07 16:32:08.508893+00:00                                              21.8
    2019-08-07 16:32:10.022784+00:00  998.8

    Table 'unsorted':
    time                              message     topic
    --------------------------------  ----------  --------------------------
    2019-08-07 16:32:10.022784+00:00  yes         home/roof/solarpanel/works


Systemd Service
---------------

.. note::

   The systemd service will only work if you have a systemd-based system
   (obviously) and you installed :mod:`mqttstore` as user as described in
   :doc:`install`.

:mod:`mqttstore` also ships a `Systemd User Service Unit`_ to run the SQLite
storage as a user service. The service just runs ``mqttstore sqlite`` when it
is started. To use it, run after the :doc:`install`:

.. code-block:: sh

   # copy over the example configuration (or create your own configuration)
   # to the default configuration location
   mkdir -p ~/.config/mqttstore
   ln -rsf ~/.local/share/mqttstore/sqlite/example.conf ~/.config/mqttstore/sqlite.conf
   # let systemctl reload the user units (remember to do this after an update, too)
   systemctl --user daemon-reload
   # check the current status of the mqttstore service
   # [The service should be displayed as inactive (dead)]
   systemctl --user status mqttstore
   # start the service
   systemctl --user start mqttstore
   # display a live log of the service
   journalctl --user -ef -u mqttstore
   # run the service at boot
   loginctl enable-linger $USER  # without this, the service only runs when you're logged in
   systemctl --user enable mqttstore


The example configuration is shipped alongside :mod:`mqttstore` and instructs
the :doc:`cli` to connect to an `MQTT`_-Broker on the local machine and just
store topics and messages in a single table.

Troubleshooting
---------------

If storing data from the MQTT brokers does not work as expected, try the
following:

- Turn verbose :ref:`cli logging` on to see what exactly is going on under the
  hood.
- If connecting to the MQTT broker is the problem, check if you can connect
  with another client:

  .. code-block:: sh

     # for Debian-based systems:
     sudo apt-get install mosquitto-clients
     # see what's going on on the broker
     mosquitto_sub -v -h BROKER -t TOPIC_PATTERN
     # publish something to the broker manually
     mosquitto_pub -h BROKER -t TOPIC -m MESSAGE


.. _paho-mqtt: http://pypi.python.org/pypi/paho-mqtt
.. _MQTT: https://en.wikipedia.org/wiki/MQTT
.. _Click: https://click.palletsprojects.com
.. _Mosquitto: https://mosquitto.org
.. _Systemd User Service Unit: https://wiki.archlinux.org/index.php/Systemd/User
