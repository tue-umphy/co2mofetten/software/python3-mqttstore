Welcome to mqttstore's documentation!
=====================================

:mod:`mqttstore` is a Python package to store `MQTT`_ data.

.. _MQTT: https://mqtt.org


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   cli
   changelog
   api/modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
