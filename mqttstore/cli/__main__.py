# system modules

# internal modules
from mqttstore.cli.commands.main import cli

# external modules

if __name__ == "__main__":
    cli()
